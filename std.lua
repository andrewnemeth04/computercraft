


Dir = {FORWARD = "forward", LEFT = "left", RIGHT = "right", DOWN = "down", BACK = "back", UP = "up"}

function digInDir (dir)
  if dir == Dir.FORWARD then
    turtle.dig()
  elseif dir == Dir.DOWN then
    turtle.digDown()
  elseif   dir == Dir.UP then
    turtle.digUp()
  end
end

function detectInDir(dir)
  if dir == Dir.FORWARD then
    return turtle.detect()
  elseif  dir == Dir.DOWN then
    return turtle.detectDown()
  elseif   dir == Dir.UP then
    return turtle.detectUp()
  end
end

function moveInDir (dir)
  print(dir)
  if dir == Dir.FORWARD then
    turtle.forward()
  elseif  dir == Dir.DOWN then
    turtle.down()
  elseif   dir == Dir.UP then
    turtle.up()
  end
end

function mineEverything(dir)
  while detectInDir(dir) do
    digInDir(dir)
  end
  while detectInDir(dir) do
    digInDir(dir)
  end
  while detectInDir(dir) do
    digInDir(dir)
  end
end

function smartMove (dir)
  if dir == Dir.LEFT then
    turtle.turnLeft()
    dir = Dir.FORWARD
  elseif dir == Dir.RIGHT then
    turtle.turnRight()
    dir = Dir.FORWARD
  elseif dir == Dir.BACK then
    turtle.turnRight()
    turtle.turnRight()
    dir = Dir.FORWARD
  end
  mineEverything(dir)
  moveInDir(dir)
end

function mineSlice()
  smartMove(Dir.FORWARD)
  smartMove(Dir.LEFT)
  smartMove(Dir.UP)
  smartMove(Dir.BACK)
  turtle.turnLeft()
  smartMove(Dir.DOWN)
end

function tunnel(length)
  for i=1,length do mineSlice() end
end

function spiralTunnel()
  chunkSize = 6
  chunkLength = 1

  while true do
    tunnel(chunkLength * chunkSize)
    turtle.turnRight()
    tunnel(chunkLength * chunkSize)
    turtle.turnRight()
    chunkLength = chunkLength + 1
  end
end

function dumpAllOfItem(itemName)
  for i=1,9 do
    	data = turtle.getItemDetail(i)
      if data.name == itemName then
         turtle.select(i)
         turtle.drop()
      end
  end
end

dumpAllOfItem("Cobblestone")
