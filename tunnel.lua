loadfile("std")
Dir = {FORWARD = "forward", LEFT = "left", RIGHT = "right", DOWN = "down", BACK = "back", UP = "up"}

function mineSlice()
  smartMove(Dir.FORWARD)
  smartMove(Dir.LEFT)
  smartMove(Dir.UP)
  smartMove(Dir.BACK)
end

mineSlice()
